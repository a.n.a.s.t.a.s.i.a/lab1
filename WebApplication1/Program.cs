
using System.Text.RegularExpressions;
 
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//builder.Services.
    
var app = builder.Build();


app.MapGet("/", () => "Hello World!");
app.MapGet("/rand_words", Handlers.RandWords);
app.MapGet("/get-urls", Handlers.GetUrls);
 
 
app.UseSwagger();
app.UseSwaggerUI();
app.Run();

public static class Handlers
{
    private static readonly Random rnd = new Random();
 
    static bool UniqueSymbols(string s)
    {
        return s.ToHashSet().Count == s.Length;
    }
 
    static string GenString(string alphabet, int length)
    {
        var ans = "";
        for (int i = 0; i < length; i++)
        {
            ans += alphabet[rnd.Next(0, length - 1)];
        }
 
        return ans;
    }
    
    public static async Task<IResult> RandWords(string alphabet, int length, int count)
    {
        if (!UniqueSymbols(alphabet) || length < 1 || count < 1)
        {
            return Results.BadRequest("error 400");
        }
 
        var listStr = new List<string>();
        for (int i = 0; i < count; i++)
        {
            listStr.Add(GenString(alphabet, length));
        }
        return Results.Ok(listStr);
    }
 
    public static async Task<IResult> GetUrls(string url)
    {
        if (url == "")  return Results.BadRequest("error 400");
        var client = new HttpClient();
        var text = "";
        try
        {
            text = client.GetAsync(url).Result.Content.ReadAsStringAsync().Result;
        }
        catch (Exception e)
        {
           return Results.BadRequest(e);
        }

        var regex = new Regex("href=\"(.+?)\"");
        var matches = regex.Matches(text);
        
        var lineList = new List<string>();
        foreach (Match match in matches)
        { 
            lineList.Add(match.Groups[1].Value);
        }
 
        return Results.Ok(lineList);
    }
}